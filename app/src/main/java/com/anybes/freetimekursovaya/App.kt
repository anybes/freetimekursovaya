package com.anybes.freetimekursovaya

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

class App : Application() {

    private var realmVersion = 0.08
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        Realm.setDefaultConfiguration(RealmConfiguration.Builder().name("freeTimev" + realmVersion).build())
    }
}