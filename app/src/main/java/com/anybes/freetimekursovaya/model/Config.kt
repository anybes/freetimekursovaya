package com.anybes.freetimekursovaya.model

import io.realm.RealmObject

open class Config : RealmObject() {
    var lastUser: User? = null
}