package com.anybes.freetimekursovaya.model

import io.realm.Realm
import io.realm.kotlin.where

class Cart {

    companion object {
        var instance = Cart()
    }

    var listProduct: ArrayList<CartProduct> = arrayListOf()
    var delivery: Boolean = false

    fun getTotalPrice(): Int {
        if (listProduct.size == 0)
            return 0
        val realm = Realm.getDefaultInstance()
        var totalSum = 0
        listProduct.forEach {
            val product = realm.where<Product>().contains("name", it.name!!).findFirst()
            totalSum += product!!.price!! * it.count!!
        }
        return totalSum
    }

    fun addProduct(product: Product, productCount: Int) {
        if (listProduct.size == 0 || listProduct.all { !it.name.equals(product.name) }) {
            var cartProduct = CartProduct()
            cartProduct.name = product.name
            cartProduct.count = productCount
            listProduct.add(cartProduct)
            return
        }


        listProduct.first { it.name.equals(product.name) }.count =
            listProduct.first { it.name.equals(product.name) }.count?.plus(productCount)

    }

    fun changeCount(product: Product, productCount: Int) {
        if (productCount == 0) {
            listProduct.remove(listProduct.first { it.name.equals(product.name) })
            return
        }

        listProduct.first { it.name.equals(product.name) }.count = productCount
    }

    fun getTotalCount(): Int {
        if (listProduct.size == 0)
            return 0

        var totalCount = 0
        listProduct.forEach { totalCount += it.count!! }
        return totalCount
    }

    fun updateAvailableProducts() {
        if (listProduct.size == 0) return
        val realm = Realm.getDefaultInstance()
        var config = realm.where<Config>().findFirst()
        var cafe = config!!.lastUser!!.lastCafe
        var bufList :ArrayList<CartProduct> = ArrayList()
        listProduct.forEach { cartProduct ->
            if (cafe!!.products.any { it.name.equals(cartProduct.name) }) {
              bufList.add(cartProduct)
            }
        }

        listProduct=  bufList

        if (delivery && !cafe!!.canDelivery) {
            delivery = false
        }
    }

    fun clearCart(){
        listProduct.clear()
        delivery = false
    }

}
