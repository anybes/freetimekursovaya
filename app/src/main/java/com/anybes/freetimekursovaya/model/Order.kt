package com.anybes.freetimekursovaya.model

import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.kotlin.where

open class Order : RealmObject() {
    @PrimaryKey
    var id: Int? = null
    var productList: RealmList<CartProduct> = RealmList()
    var delivery: Boolean = false
    var firstName: String? = null
    var secondName: String? = null
    var street: String? = null
    var apartment: String? = null
    var houseNumber: String? = null

    fun getTotalPrice(): Int {
        if (productList.size == 0)
            return 0
        val realm = Realm.getDefaultInstance()
        var totalSum = 0
        productList.forEach {
            val product = realm.where<Product>().contains("name", it.name!!).findFirst()
            totalSum += product!!.price!! * it.count!!
        }
        return totalSum
    }
}