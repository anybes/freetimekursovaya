package com.anybes.freetimekursovaya.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class User : RealmObject() {
    @PrimaryKey
    var phoneNumber: String? = null
    var password: String? = null
    var firstName: String? = null
    var secondName: String? = null
    var street: String? = null
    var apartment: String? = null
    var houseNumber: String? = null
    var lastCafe: Cafe? = null
    var orderList: RealmList<Order> = RealmList()
}