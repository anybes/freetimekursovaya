package com.anybes.freetimekursovaya.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Product : RealmObject() {
    @PrimaryKey
    var name: String? = null
    var description: String? = null
    var composition: String? = null
    var category: String? = null
    var price: Int? = null
    var count: Int? = null
    var imageId: Int? = null
}