package com.anybes.freetimekursovaya.model

import io.realm.RealmObject


open class CartProduct:RealmObject(){
    var name: String? = null
    var count: Int? = null
}