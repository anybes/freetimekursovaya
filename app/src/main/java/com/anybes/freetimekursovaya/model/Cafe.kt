package com.anybes.freetimekursovaya.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Cafe : RealmObject() {
    @PrimaryKey
    var id: String? = null
    var canDelivery: Boolean = false
    var workTime: String? = null
    var street: String? = null
    var houseNumber: String? = null
    var products: RealmList<Product> = RealmList()
}