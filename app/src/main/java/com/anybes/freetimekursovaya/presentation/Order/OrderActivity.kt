package com.anybes.freetimekursovaya.presentation.Order

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.common.OrderRecyclerAdapter
import com.anybes.freetimekursovaya.model.Config
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_order.*

class OrderActivity : AppCompatActivity() {

    private lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)
        initDB()
        initView()
    }

    private fun initView() {
        var config = realm.where<Config>().findFirst()
        var order = config!!.lastUser!!.orderList!!.last()

        orderNumber.text = String.format(getString(R.string.order_number_format, order!!.id))

        var llManager = LinearLayoutManager(this)
        productRecycler.apply {
            hasFixedSize()
            layoutManager = llManager
            adapter = OrderRecyclerAdapter(order.productList.toList())
        }

        totalPrice.text =
            String.format(getString(R.string.total_price_format, order.getTotalPrice()))

        if (order.delivery) {
            deliveryType.text = String.format(
                getString(R.string.type_delivery_format),
                getString(R.string.delivery)
            )

            address.visibility = View.VISIBLE
            if (order.apartment == null) {
                address.text = String.format(
                    getString(R.string.address_format_non_apartment),
                    order.street,
                    order.houseNumber
                )
            } else {
                address.text = String.format(
                    getString(R.string.address_format),
                    order.street,
                    order.houseNumber,
                    order.apartment
                )
            }
        } else {
            deliveryType.text = String.format(
                getString(R.string.type_delivery_format),
                getString(R.string.pickup)
            )
            address.visibility = View.GONE
        }

        acceptBtn.setOnClickListener { finish() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun initDB() {
        realm = Realm.getDefaultInstance()
    }
}