package com.anybes.freetimekursovaya.presentation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.model.Cart
import com.anybes.freetimekursovaya.presentation.Catalog.CategoryActivity
import com.anybes.freetimekursovaya.presentation.Order.CartActivity
import com.anybes.freetimekursovaya.presentation.Profile.ProfileActivity
import com.google.android.material.bottomnavigation.BottomNavigationView

open class BaseActivity(val navId: Int) : AppCompatActivity() {

    private lateinit var _navbar: BottomNavigationView
    private fun setupNavbar() {
        _navbar = findViewById(R.id.bottomNavbar)
        _navbar.setOnNavigationItemSelectedListener {
            val nextActivity =
                when (it.itemId) {
                    R.id.nav_item_cart -> CartActivity::class.java
                    R.id.nav_item_catalog -> CategoryActivity::class.java
                    R.id.nav_item_profile -> ProfileActivity::class.java
                    else -> {
                        null
                    }
                }

            if (nextActivity != null) {
                val intent = Intent(this, nextActivity)
                startActivity(intent)
                true
            } else {
                false
            }
        }


        if (_navbar.menu.size() >navId ) {
            _navbar.menu.getItem(navId).isChecked = true
            _navbar.menu.getItem(navId).isEnabled = false
        }

    }

    override fun onResume() {
        super.onResume()
        update()
    }


    open fun update() {
        setupNavbar()
        updateCartBadge()
    }

    fun updateCartBadge() {
        var totalCount = Cart.instance.getTotalCount()
        if (totalCount == 0) {
            _navbar.removeBadge(R.id.nav_item_cart)
            return
        } else {
            _navbar.getOrCreateBadge(R.id.nav_item_cart)
                .apply { number = Cart.instance.getTotalCount() }
        }
    }
}