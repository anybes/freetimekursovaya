package com.anybes.freetimekursovaya.presentation.Profile

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.model.Cafe
import com.anybes.freetimekursovaya.model.Cart
import com.anybes.freetimekursovaya.model.Config
import com.anybes.freetimekursovaya.presentation.BaseActivity
import com.anybes.freetimekursovaya.presentation.Authorization.SignInActivity
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_profile.*
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.slots.PredefinedSlots
import ru.tinkoff.decoro.watchers.MaskFormatWatcher
import java.util.*


class ProfileActivity : BaseActivity(2) {

    private lateinit var realm: Realm
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        initDB()
        initView()
    }

    private fun initDB() {
        realm = Realm.getDefaultInstance()
    }

    override fun update() {
        super.update()
        updateView()
    }

    private fun updateView() {
        val config = realm.where<Config>().findFirst()
        val user = config!!.lastUser

        user?.firstName.let { firstName.setText(it) }
        user?.secondName.let { secondName.setText(it) }

        user?.phoneNumber.let { phoneNumber.setText(it) }
        user?.password.let { password.setText(it) }
        user?.street.let { street.setText(it) }
        user?.houseNumber.let { house.setText(it) }
        user?.apartment.let { apartment.setText(it) }

        val cafes = realm.where<Cafe>().findAll()
        val spinerItems: ArrayList<String> = ArrayList()
        cafes.forEach { spinerItems.add(it.id!!) }
        val spinnerAdapter =
            ArrayAdapter<String>(this, R.layout.cafe_spinner_item, R.id.cafeName, spinerItems)
        cafeSpinner.adapter = spinnerAdapter
        val selectionIndex = spinerItems.indexOf(config.lastUser!!.lastCafe!!.id)
        cafeSpinner.setSelection(selectionIndex)
    }

    private fun initView() {

        val mask = MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER)
        mask.isHideHardcodedHead = false
        val formatWatcher = MaskFormatWatcher(mask)
        formatWatcher.installOn(phoneNumber)

        saveBtn.setOnClickListener {
            if (password.text.isNullOrEmpty()) {
                Toast.makeText(this, "Enter phone number or password", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            realm.beginTransaction()
            val config = realm.where<Config>().findFirst()
            config!!.lastUser!!.firstName = firstName.text.toString()
            config.lastUser!!.secondName = secondName.text.toString()
            config.lastUser!!.street = street.text.toString()
            config.lastUser!!.houseNumber = house.text.toString()
            config.lastUser!!.apartment = apartment.text.toString()
            config.lastUser!!.password = password.text.toString()

            val cafe =
                realm.where<Cafe>().contains("id", cafeSpinner.selectedItem.toString()).findFirst()
            config.lastUser!!.lastCafe = cafe!!

            realm.commitTransaction()

            Cart.instance.updateAvailableProducts()
            updateCartBadge()
        }

        logoutBtn.setOnClickListener {
            realm.beginTransaction()
            val config = realm.where<Config>().findFirst()
            config?.lastUser = null
            realm.commitTransaction()
            Cart.instance.clearCart()
            val intent = Intent(this, SignInActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
    }

}