package com.anybes.freetimekursovaya.presentation.Catalog

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.common.ProductsRecyclerAdapter
import com.anybes.freetimekursovaya.model.Config
import com.anybes.freetimekursovaya.presentation.BaseActivity
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_products.*

class ProductsActivity : BaseActivity(4) {
    private lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)

        var categoryName = intent.getStringExtra("categoryName")
        realm = Realm.getDefaultInstance()

        var llManager = LinearLayoutManager(this)

        productsRecycler.apply {
            hasFixedSize()
            layoutManager = llManager
            adapter =
                ProductsRecyclerAdapter(
                    realm.where<Config>().findFirst()!!.lastUser!!.lastCafe!!.products.where().contains(
                        "category",
                        categoryName!!
                    ).findAll(), this@ProductsActivity
                )
        }
    }
}