package com.anybes.freetimekursovaya.presentation.Catalog

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.model.Cart
import com.anybes.freetimekursovaya.model.Product
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_product.*

class ProductActivity : AppCompatActivity() {

    private lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)
        var name = intent.getStringExtra("productName")
        title = name

        realm = Realm.getDefaultInstance()
        var product = realm.where<Product>().contains("name", name).findFirst()

        productName.text = name

        productImg.setImageResource(product!!.imageId!!)

        productDescription.text = product!!.description
        productComposition.text = product!!.composition

        var count = 1
        productCount.text = count.toString()

        minusCountBtn.setOnClickListener {
            if (count == 1) {
                return@setOnClickListener
            }

            count--
            productCount.text = count.toString()
        }

        plusCountBtn.setOnClickListener {
            var remainingCount = product!!.count
            if (remainingCount!!.minus(count + 1) < 0) {
                return@setOnClickListener
            }

            count++
            productCount.text = count.toString()
        }

        addToCartBtn.setOnClickListener {
            if (product!!.count!! > count) {
                Cart.instance.addProduct(product!!, count)
                count = 1
                productCount.text = count.toString()

            } else {
                Toast.makeText(this, "Нет в наличии в таком количестве", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }
}