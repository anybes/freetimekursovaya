package com.anybes.freetimekursovaya.presentation

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.model.Cafe
import com.anybes.freetimekursovaya.model.Config
import com.anybes.freetimekursovaya.model.User
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_signup.*
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.slots.PredefinedSlots
import ru.tinkoff.decoro.watchers.MaskFormatWatcher

class SignUpActivity : AppCompatActivity() {

    private lateinit var realm: Realm
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        initDB()
        initView()
    }


    private fun initDB() {
        realm = Realm.getDefaultInstance()
    }

    private fun initView() {
        val mask = MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER)
        mask.isHideHardcodedHead = false
        val formatWatcher = MaskFormatWatcher(mask)
        formatWatcher.installOn(phoneNumber)

        signinBtn.setOnClickListener {
            var intent = Intent(this, SignInActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)

        }

        acceptBtn.setOnClickListener {
            signUp(formatWatcher.mask.toUnformattedString())
        }

    }

    private fun signUp(phone: String) {

        if (phone.length > 2 && !password.text.toString().isEmpty()) {
            var user = getUserBy(phone)
            if (user == null) {
                realm.beginTransaction()
                var newUser = realm.createObject(
                    User::class.java,
                    phone
                )
                newUser.password = password.text.toString()
                newUser.lastCafe = realm.where<Cafe>().findFirst()
                realm.where<Config>().findFirst()!!.lastUser = newUser
                realm.commitTransaction()
                startActivity(Intent(this, CategoryActivity::class.java))
                finish()
            } else {
                Toast.makeText(this, "user exist", Toast.LENGTH_LONG).show()
            }
        } else {
            Toast.makeText(this, "Enter all fields", Toast.LENGTH_LONG).show()
        }
    }

    private fun getUserBy(phone: String) =
        realm.where<User>().contains("phoneNumber", phone).findFirst()
}