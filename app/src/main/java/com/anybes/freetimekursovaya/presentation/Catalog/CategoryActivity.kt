package com.anybes.freetimekursovaya.presentation.Catalog

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.common.CategoryRecyclerAdapter
import com.anybes.freetimekursovaya.model.Category
import com.anybes.freetimekursovaya.presentation.BaseActivity
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_catalog.*

class CategoryActivity : BaseActivity(0) {

    private lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catalog)

        realm = Realm.getDefaultInstance()

        var llManager = LinearLayoutManager(this)

        categoryRecycler.apply {
            hasFixedSize()
            layoutManager = llManager
            adapter =
                CategoryRecyclerAdapter(realm.where<Category>().findAll(), this@CategoryActivity)
        }
    }

}