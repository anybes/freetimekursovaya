package com.anybes.freetimekursovaya.presentation.Authorization

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.model.Config
import com.anybes.freetimekursovaya.model.User
import com.anybes.freetimekursovaya.presentation.Catalog.CategoryActivity
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_signin.*
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.slots.PredefinedSlots
import ru.tinkoff.decoro.watchers.MaskFormatWatcher


class SignInActivity : AppCompatActivity() {

    private lateinit var realm: Realm
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        initDB()
        initView()
    }

    private fun initDB() {
        realm = Realm.getDefaultInstance()
    }

    private fun initView() {
        val mask = MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER)
        mask.isHideHardcodedHead = false
        val formatWatcher = MaskFormatWatcher(mask)
        formatWatcher.installOn(phoneNumber)

        acceptBtn.setOnClickListener {
            signIn(formatWatcher.mask.toUnformattedString())
        }

        signupBtn.setOnClickListener {
            var intent = Intent(this, SignUpActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }

    }

    private fun signIn(phone: String) {

        if (phone.length > 2 && !password.text.toString().isEmpty()) {
            var user = getUserBy(phone)
            if (user == null) {
                Toast.makeText(this, "user with this phone does not exist", Toast.LENGTH_LONG)
                    .show()
            } else {
                if (user.password!!.equals(password.text.toString(), true)) {
                    realm.beginTransaction()
                    realm.where<Config>().findFirst()!!.lastUser = user
                    realm.commitTransaction()
                    startActivity(Intent(this, CategoryActivity::class.java))
                    finish()
                } else {
                    Toast.makeText(this, "password invalid", Toast.LENGTH_LONG)
                        .show()
                }
            }
        } else {
            Toast.makeText(this, "Enter all fields", Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun getUserBy(phone: String) =
        realm.where<User>().contains("phoneNumber", phone).findFirst()

}
