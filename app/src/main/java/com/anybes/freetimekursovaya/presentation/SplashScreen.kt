package com.anybes.freetimekursovaya.presentation

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.model.*
import com.anybes.freetimekursovaya.presentation.Authorization.SignInActivity
import com.anybes.freetimekursovaya.presentation.Authorization.SignUpActivity
import com.anybes.freetimekursovaya.presentation.Catalog.CategoryActivity
import io.realm.Realm
import io.realm.kotlin.where

class SplashScreen : AppCompatActivity() {
    private lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        realm = Realm.getDefaultInstance()

        var config = realm.where<Config>().findFirst()
        if (config == null) {
            realm.beginTransaction()
            realm.createObject(Config::class.java)
            realm.commitTransaction()
            config = realm.where<Config>().findFirst()
        }

        createFakeData()

        if (realm.where<User>().findAll().size == 0) {
            startActivity(Intent(this, SignUpActivity::class.java))
        } else {
            var lastUser = config!!.lastUser
            if (lastUser == null) {
                startActivity(Intent(this, SignInActivity::class.java))
            } else {
                startActivity(Intent(this, CategoryActivity::class.java))
            }
        }

        Thread.sleep(5000)
        finish()
    }

    private fun createFakeData() {
        realm.beginTransaction()

        var tweester = Product()
        tweester.name = "Твистер"
        tweester.category = "Гамбургеры"
        tweester.description = "Состав: Лаваш, лист салата, куриные палочки, огурец, лук, соус."
        tweester.composition = "Состав: Лаваш, лист салата, куриные палочки, огурец, лук, соус."
        tweester.imageId = R.drawable.twister
        tweester.price = 125
        tweester.count = 1000

        if (!checkExistProduct(tweester.name.toString())) {
            var realmProiduct = realm.createObject(Product::class.java, tweester.name)
            realmProiduct.category = tweester.category
            realmProiduct.description = tweester.description
            realmProiduct.composition = tweester.composition
            realmProiduct.imageId = tweester.imageId
            realmProiduct.price = tweester.price
            realmProiduct.count = tweester.count
        }

        var chikenLite = Product()
        chikenLite.name = "Чикен Лайт"
        chikenLite.category = "Гамбургеры"
        chikenLite.description =
            "Состав: Булочка с кунжутом, лист салата, куриная котлета, огурец, лук, соус, помидор."
        chikenLite.composition =
            "Состав: Булочка с кунжутом, лист салата, куриная котлета, огурец, лук, соус, помидор."
        chikenLite.imageId = R.drawable.chiken_lite
        chikenLite.price = 119
        chikenLite.count = 1000

        if (!checkExistProduct(chikenLite.name.toString())) {
            var realmProduct = realm.createObject(Product::class.java, chikenLite.name)
            realmProduct.category = chikenLite.category
            realmProduct.description = chikenLite.description
            realmProduct.composition =
                chikenLite.composition
            realmProduct.imageId = chikenLite.imageId
            realmProduct.price = chikenLite.price
            realmProduct.count = chikenLite.count
        }

        var cafe: Cafe
        if (!checkExistCafe("Острова")) {
            cafe = realm.createObject(Cafe::class.java, "Острова")
            cafe.houseNumber = "114"
            cafe.canDelivery = true
            cafe.street = "Мухина"
            cafe.workTime = "10:00 - 22:00"
            cafe.products.add(tweester)
            cafe.products.add(chikenLite)

        }


        if (!checkExistCafe("Амурская")) {
            cafe = realm.createObject(Cafe::class.java, "Амурская")
            cafe.houseNumber = "201а"
            cafe.canDelivery = false
            cafe.street = "Амурская"
            cafe.workTime = "10:00 - 23:00"
            cafe.products.add(tweester)
        }


        var category: Category
        if (!checkExistCategory("Гамбургеры")) {
            category = realm.createObject(Category::class.java, "Гамбургеры")
            category.imageId = R.drawable.chiken_lite
        }

        realm.commitTransaction()
    }

    private fun checkExistCafe(id: String) =
        realm.where<Cafe>().contains("id", id).findFirst() != null

    private fun checkExistProduct(name: String) =
        realm.where<Product>().contains("name", name).findFirst() != null

    private fun checkExistCategory(name: String) =
        realm.where<Category>().contains("name", name).findFirst() != null


}

