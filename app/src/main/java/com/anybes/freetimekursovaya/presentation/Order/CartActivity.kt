package com.anybes.freetimekursovaya.presentation.Order

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.common.CartRecyclerAdapter
import com.anybes.freetimekursovaya.model.Cart
import com.anybes.freetimekursovaya.model.Config
import com.anybes.freetimekursovaya.model.Order
import com.anybes.freetimekursovaya.presentation.BaseActivity
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_cart.*


class CartActivity : BaseActivity(1) {

    private lateinit var realm: Realm
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        initDB()

        var llManager = LinearLayoutManager(this)

        cartRecycler.apply {
            hasFixedSize()
            layoutManager = llManager
            adapter = CartRecyclerAdapter(Cart.instance.listProduct, this@CartActivity)
        }

        deliverySpiner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                itemSelected: View, selectedItemPosition: Int, selectedId: Long
            ) {
                Cart.instance.delivery = selectedItemPosition == 0

            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        })

        acceptOrderBtn.setOnClickListener {
            CreateOrder()
        }
    }

    private fun CreateOrder() {
        var config = realm.where<Config>().findFirst()
        var user = config!!.lastUser!!
        if (Cart.instance.delivery && (user.houseNumber.isNullOrEmpty()
                    || user.street.isNullOrEmpty() || user.firstName.isNullOrEmpty() || user.secondName.isNullOrEmpty())
        ) {

            Toast.makeText(this, "Please enter information about you", Toast.LENGTH_SHORT)
                .show()

        } else {
            var orderes = realm.where<Order>().findAll()
            var orderNum = 1
            if (orderes.size != 0) {
                orderNum = orderes.max("id")!!.toInt() + 1
            }
            realm.beginTransaction()

            var order: Order = realm.createObject(Order::class.java, orderNum)
            order.delivery = Cart.instance.delivery
            order.apartment = user.apartment
            order.firstName = user.firstName
            order.secondName = user.secondName
            order.houseNumber = user.houseNumber
            order.street = user.street
            order.productList.addAll(Cart.instance.listProduct)

            user.orderList.add(order)

            realm.commitTransaction()

            Cart.instance.clearCart()
            update()
            var intent = Intent(this, OrderActivity::class.java)
            startActivity(intent)
        }

    }

    private fun initDB() {
        realm = Realm.getDefaultInstance()
    }

    override fun update() {
        super.update()
        totalPrice.text = String.format(
            getString(R.string.total_price_format),
            Cart.instance.getTotalPrice()
        )
        cartRecycler.adapter?.notifyDataSetChanged()
        updateLayerVisible()
        updateView()
    }

    private fun updateLayerVisible() {
        if (Cart.instance.listProduct.size == 0) {
            emptyLayer.visibility = View.VISIBLE
            notEmptylayer.visibility = View.GONE
        } else {
            emptyLayer.visibility = View.GONE
            notEmptylayer.visibility = View.VISIBLE
        }
    }

    private fun updateView() {

        var config = realm.where<Config>().findFirst()
        if (config!!.lastUser!!.lastCafe!!.canDelivery) {
            val spinerItems = resources.getStringArray(R.array.deleveryList)
            val spinnerAdapter =
                ArrayAdapter<String>(this, R.layout.cafe_spinner_item, R.id.cafeName, spinerItems)
            deliverySpiner.adapter = spinnerAdapter

            if (Cart.instance.delivery) {
                deliverySpiner.setSelection(0)
            } else {
                deliverySpiner.setSelection(1)
            }


        } else {
            deliverySpiner.visibility = View.GONE
        }

    }
}