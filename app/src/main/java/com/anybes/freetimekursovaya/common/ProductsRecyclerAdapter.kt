package com.anybes.freetimekursovaya.common

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.model.Cart
import com.anybes.freetimekursovaya.model.Product
import com.anybes.freetimekursovaya.presentation.BaseActivity
import com.anybes.freetimekursovaya.presentation.Catalog.ProductActivity
import kotlinx.android.synthetic.main.list_item_product.view.*

class ProductsRecyclerAdapter(
    private val productList: List<Product>,
    private val parentActivity: BaseActivity
) :
    RecyclerView.Adapter<ProductsRecyclerAdapter.ProductsRecyclerEventHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsRecyclerEventHolder =
        ProductsRecyclerEventHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_product, parent, false
            ) as CardView
        )

    override fun getItemCount(): Int = productList.size

    override fun onBindViewHolder(holder: ProductsRecyclerEventHolder, position: Int) {
        var context = holder.view.context
        productList[position].imageId?.let { holder.view.productImg.setImageResource(it) }
        holder.view.productName.text = productList[position].name!!
        holder.view.productComposition.text = productList[position].composition!!
        holder.view.productPrice.text = String.format(
            context.getString(R.string.price_format),
            productList[position].price
        )

        holder.view.addToBasket.setOnClickListener {
            if (productList[position].count!! > 0) {
                Cart.instance.addProduct(
                    productList[position],
                    1
                )
                parentActivity.updateCartBadge()


            } else {
                Toast.makeText(context, "Нет в наличии в таком количестве", Toast.LENGTH_LONG)
                    .show()
            }

        }

        holder.view.setOnClickListener {
            var intent = Intent(
                context,
                ProductActivity::class.java
            )
            intent.putExtra("productName", productList[position].name)
            context.startActivity(
                intent
            )
        }
    }

    class ProductsRecyclerEventHolder(val view: View) : RecyclerView.ViewHolder(view)
}