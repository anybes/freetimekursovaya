package com.anybes.freetimekursovaya.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.model.CartProduct
import com.anybes.freetimekursovaya.model.Product
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.list_item_order.view.*

class OrderRecyclerAdapter(
    private val productList: List<CartProduct>
) :
    RecyclerView.Adapter<OrderRecyclerAdapter.OrderRecyclerEventHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderRecyclerEventHolder =
        OrderRecyclerEventHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_order, parent, false
            ) as CardView
        )

    override fun getItemCount(): Int = productList.size

    override fun onBindViewHolder(holder: OrderRecyclerEventHolder, position: Int) {
        var context = holder.view.context
        var product =
            Realm.getDefaultInstance().where<Product>().contains("name", productList[position].name)
                .findFirst()

        product?.name.let { holder.view.productName.text = it!! }
        product?.price.let {
            holder.view.price.text = String.format(
                context.getString(R.string.price_format),
                (productList[position].count?.times(it!!))
            )
        }

//Todo need change string
        holder.view.count.text = String.format(
            "%d шт.",
            productList[position].count
        )

    }


    class OrderRecyclerEventHolder(val view: View) : RecyclerView.ViewHolder(view)
}