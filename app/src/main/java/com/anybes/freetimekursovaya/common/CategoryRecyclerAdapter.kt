package com.anybes.freetimekursovaya.common

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.model.Category
import com.anybes.freetimekursovaya.model.Config
import com.anybes.freetimekursovaya.presentation.BaseActivity
import com.anybes.freetimekursovaya.presentation.Catalog.ProductsActivity
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.list_item_category.view.*

class CategoryRecyclerAdapter(
    private val categoryList: List<Category>,
    private val parentActivity: BaseActivity
) :
    RecyclerView.Adapter<CategoryRecyclerAdapter.CategoryRecyclerEventHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryRecyclerEventHolder =
        CategoryRecyclerEventHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_category, parent, false
            ) as CardView
        )

    override fun getItemCount(): Int = categoryList.size

    override fun onBindViewHolder(holder: CategoryRecyclerEventHolder, position: Int) {
        var context = holder.view.context
        holder.view.categoryName.text = categoryList[position].name
        categoryList[position].imageId?.let { holder.view.categoryImg.setImageResource(it) }
        if (Realm.getDefaultInstance().where<Config>().findFirst()!!.lastUser!!.lastCafe!!.products.all {
                !it.category.equals(
                    categoryList[position].name
                )
            }) {
            holder.view.visibility = View.GONE
        }
        holder.view.setOnClickListener {
            var intent = Intent(
                context,
                ProductsActivity::class.java
            )
            intent.putExtra("categoryName", categoryList[position].name)
            context.startActivity(
                intent
            )
        }
    }

    class CategoryRecyclerEventHolder(val view: View) : RecyclerView.ViewHolder(view)
}