package com.anybes.freetimekursovaya.common

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.anybes.freetimekursovaya.R
import com.anybes.freetimekursovaya.model.Cart
import com.anybes.freetimekursovaya.model.CartProduct
import com.anybes.freetimekursovaya.model.Product
import com.anybes.freetimekursovaya.presentation.Order.CartActivity
import com.anybes.freetimekursovaya.presentation.Catalog.ProductActivity
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.list_item_cart_product.view.*

class CartRecyclerAdapter(
    private val productList: ArrayList<CartProduct>,
    private val parentActivity: CartActivity
) :
    RecyclerView.Adapter<CartRecyclerAdapter.CartRecyclerEventHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartRecyclerEventHolder =
        CartRecyclerEventHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_cart_product, parent, false
            ) as CardView
        )

    override fun getItemCount(): Int = productList.size

    override fun onBindViewHolder(holder: CartRecyclerEventHolder, position: Int) {
        var context = holder.view.context
        var product =
            Realm.getDefaultInstance().where<Product>().contains("name", productList[position].name)
                .findFirst()

        product?.imageId?.let { holder.view.productImg.setImageResource(it) }

        holder.view.productName.text = product?.name
        holder.view.productPrice.text = String.format(
            context.getString(R.string.price_format),
            product?.price
        )

        holder.view.productCount.text = String.format(
            context.getString(R.string.count_format),
            productList[position].count
        )

        holder.view.totalPrice.text = String.format(
            context.getString(R.string.total_price_format),
            (productList[position].count?.times(product?.price!!))
        )

        holder.view.removeBtn.setOnClickListener {
            Cart.instance.listProduct.remove(productList[position])
            parentActivity.update()
        }

        holder.view.setOnClickListener {
            var intent = Intent(
                context,
                ProductActivity::class.java
            )
            intent.putExtra("productName", productList[position].name)
            context.startActivity(
                intent
            )
        }
    }


    class CartRecyclerEventHolder(val view: View) : RecyclerView.ViewHolder(view)
}